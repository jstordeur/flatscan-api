#include "parameters.h"

using namespace std;

ostream& operator<<(ostream& os, const Parameters& p) {
	os << "[Parameters]\n";
	os << "  CTN : ";
	switch(p.ctn()) {
		case ENABLE :
			os << "enable\n";
			break;

		case DISABLE :
			os << "disable\n";
			break;

		default :
			os << "WTF " << hex << uppercase << std::setfill('0') << std::setw(2) << (int) p.data_[BYTE_CTN] << endl << dec;
			break;
	}

	os << "  MDI : ";
	switch(p.mdi()) {
		case MDI_DIST :
			os << "distance\n";
			break;

		case MDI_REMI :
			os << "remission\n";
			break;

		case MDI_DISTREMI :
			os << "distance and remission\n";
			break;

		default :
			os << "WTF " << hex << uppercase << std::setfill('0') << std::setw(2) << (int) p.data_[BYTE_MDI] << endl << dec;
			break;
	}

	os << "  ACQUISITION : ";
	switch(p.acquisition()) {
		case HS :
			os << "high speed\n";
			break;

		case HD :
			os << "high definition\n";
			break;

		default :
			os << "WTF " << hex << uppercase << std::setfill('0') << std::setw(2) << (int) p.data_[BYTE_ACQUISITION] << endl << dec;
			break;
	}

	os << "  OPTIMISATION : ";
	switch(p.optimisation()) {
		case OPTI_DISBALE :
			os << "disable\n";
			break;

		case OPTI_2_5 :
			os << "between 0 and 2.5m\n";
			break;

		case OPTI_3_0 :
			os << "between 0 and 3.0m\n";
			break;

		case OPTI_3_5 :
			os << "between 0 and 3.5m\n";
			break;

		case OPTI_MAX :
			os << "beyond 3.5m\n";
			break;

		default :
			os << "WTF " << hex << uppercase << std::setfill('0') << std::setw(2) << (int) p.data_[BYTE_OPTIMISATION] << endl << dec;
			break;
	}

	os << "  SPOT : " << p.number_spot() << endl;

	os << "  ANGLE_FIST : " << p.angle()[0] << endl;
	os << "  ANGLE_LAST : " << p.angle()[1] << endl;

	os << "  CAN_CNTR : ";
	switch(p.can_cntr()) {
		case ENABLE :
			os << "enable\n";
			break;

		case DISABLE :
			os << "disable\n";
			break;

		default :
			os << "WTF " << hex << uppercase << std::setfill('0') << std::setw(2) << (int) p.data_[BYTE_CAN_CNTR] << endl;
			break;
	}

	os << "  FACET : ";
	switch(p.facet()) {
		case ENABLE :
			os << "enable\n";
			break;

		case DISABLE :
			os << "disable\n";
			break;

		default :
			os << "WTF " << hex << uppercase << std::setfill('0') << std::setw(2) << (int) p.data_[BYTE_FACET] << endl << dec;
			break;
	}

	os << "  AVERAGING : ";
	switch(p.averaging()) {
		case AVRG_DISABLE :
			os << "disable";
			break;

		case AVRG_3PTS :
			os << "average on 3 points in time";
			break;

		case AVRG_3PTS_NEIGHBOURS :
			os << "average on 3 points in time and 2 neightbourg";
			break;

		case AVRG_5PTS :
			os << "average on 5 points in time";
			break;

		case AVRG_5PTS_NEIGHBOURS :
			os << "average on 5 points in time and 2 neightbourg";
			break;

		default :
			os << "WTF " << hex << uppercase << std::setfill('0') << std::setw(2) << (int) p.data_[BYTE_AVERAGING] << dec;
			break;
	}

    return os;
}

Parameters::Parameters() {
	assert((PARAMETERS_SIZE == BYTE_AVERAGING + 1));

	data_.fill(0);

	data_[BYTE_ACQUISITION] = HD;
}

/**
 * Getters
 */
State Parameters::ctn() const {
	return static_cast<State>(data_[BYTE_CTN]);
}

Mdi Parameters::mdi() const {
	return static_cast<Mdi>(data_[BYTE_MDI]);
}

Acquisition Parameters::acquisition() const {
	return static_cast<Acquisition>(data_[BYTE_ACQUISITION]);
}

Optimisation Parameters::optimisation() const {
	return static_cast<Optimisation>(data_[BYTE_OPTIMISATION]);
}

uint16_t Parameters::number_spot() const {
	return data_[BYTE_SPOT] | data_[BYTE_SPOT + 1] << 8;
}

std::array<uint16_t, 2> Parameters::angle() const {
	std::array<uint16_t, 2> a;
	a[0] = (data_[BYTE_ANGLE_FIRST] | data_[BYTE_ANGLE_FIRST + 1] << 8) / 100;
	a[1] = (data_[BYTE_ANGLE_LAST] | data_[BYTE_ANGLE_LAST + 1] << 8) / 100;
	return a;
}

State Parameters::can_cntr() const {
	return static_cast<State>(data_[BYTE_CAN_CNTR]);
}

State Parameters::facet() const {
	return static_cast<State>(data_[BYTE_FACET]);
}

Averaging Parameters::averaging() const {
	return static_cast<Averaging>(data_[BYTE_AVERAGING]);
}

float Parameters::scan_time() const {
	return acquisition() == HS ? .01075 : .043;
}

std::array<uint8_t, PARAMETERS_SIZE> Parameters::data() const {
	return data_;
}

std::vector<uint8_t> Parameters::to_vector() const {
	return std::vector<uint8_t> (data_.begin(), data_.end());
}

/**
 * Setters
 */
bool Parameters::set_ctn(State state) {
	data_[BYTE_CTN] = static_cast<uint8_t>(state);

	return true;
}

bool Parameters::set_mdi(Mdi mode) {
	data_[BYTE_MDI] = static_cast<uint8_t>(mode);

	return true;
}

bool Parameters::set_acquisition(Acquisition mode) {
	data_[BYTE_ACQUISITION] = static_cast<uint8_t>(mode);

	return true;
}

bool Parameters::set_optimisation(Optimisation mode) {
	data_[BYTE_OPTIMISATION] = static_cast<uint8_t>(mode);

	return true;
}

bool Parameters::set_number_spot(uint16_t number) {
	if (data_[BYTE_ACQUISITION] == HS && number > 100) {
		cout << "[" << __PRETTY_FUNCTION__ << "] In high speed mode : cannot set number of spot greater than 100\n";
		return false;
	}

	if (data_[BYTE_ACQUISITION] == HD && number > 400) {
		cout << "[" << __PRETTY_FUNCTION__ << "] In high definition mode : cannot set number of spot greater than 400\n";
		return false;
	}

	if (data_[BYTE_ACQUISITION] == HD && number % 4 != 0) {
		cout << "[" << __PRETTY_FUNCTION__ << "] In high definition mode : number of sport must be multiple of 4\n";
		return false;
	}

	data_[BYTE_SPOT] = number & 0xFF;
	data_[BYTE_SPOT + 1] = (number >> 8) & 0xFF;

	return true;
}

bool Parameters::set_angle(uint16_t min, uint16_t max) {
	if (min >= max) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] Cannot set ANGLE_FIST >= ANGLE_LAST\n";
		return false;
	}

	if (min > 108) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] Cannot set ANGLE_FIST > 108deg\n";
		return false;
	}

	if (max > 108) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] Cannot set ANGLE_LAST > 108deg\n";
		return false;
	}

	// resolution is set as .01°
	max *= 100;
	min *= 100;

	data_[BYTE_ANGLE_FIRST] = min & 0xFF;
	data_[BYTE_ANGLE_FIRST + 1] = (min >> 8) & 0xFF;

	data_[BYTE_ANGLE_LAST] = max & 0xFF;
	data_[BYTE_ANGLE_LAST + 1] = (max >> 8) & 0xFF;

	return true;
}

bool Parameters::set_can_cntr(State state = DISABLE) {
	data_[BYTE_CAN_CNTR] = static_cast<uint8_t>(state);

	return true;
}

bool Parameters::set_facet(State state = DISABLE) {
	data_[BYTE_FACET] = static_cast<uint8_t>(state);

	return true;
}

bool Parameters::set_averaging(Averaging mode) {
	data_[BYTE_AVERAGING] = static_cast<uint8_t>(mode);

	return true;
}

bool Parameters::set_data(std::vector<uint8_t> vec) {
	if (vec.size() == PARAMETERS_SIZE + BYTE_HEADER_DATA) {
		copy_n(vec.begin() + BYTE_HEADER_DATA, PARAMETERS_SIZE, data_.begin());
		return true;
	}
	else {
		return false;
	}
}

bool Parameters::set_data(std::array<uint8_t, PARAMETERS_SIZE> arr) {
	data_ = arr;

	return true;
}

bool Parameters::ill_formed(const uint32_t& data) {
	if (data == 0) {
		return false;
	}

	if (data & BYTE_VERIF_CTN) {
		std::cout << "CTN field not valid\n";
	}

	if (data & BYTE_VERIF_MDI) {
		std::cout << "MDI field not valid\n";
	}

	if (data & BYTE_VERIF_ACQUISITION) {
		std::cout << "ACQUISITION field not valid\n";
	}

	if (data & BYTE_VERIF_OPTIMISATION) {
		std::cout << "OPTIMISATION field not valid\n";
	}

	if (data & BYTE_VERIF_SPOT) {
		std::cout << "SPOT field not valid\n";
	}

	if (data & BYTE_VERIF_ANGLE_FIRST) {
		std::cout << "ANGLE_FIST field not valid\n";
	}

	if (data & BYTE_VERIF_ANGLE_LAST) {
		std::cout << "ANGLE_LAST field not valid\n";
	}

	if (data & BYTE_VERIF_CAN_CNTR) {
		std::cout << "CAN_CNTR field not valid\n";
	}

	if (data & BYTE_VERIF_HEARTBEAT) {
		std::cout << "HEARTBEAT field not valid\n";
	}

	if (data & BYTE_VERIF_FACET) {
		std::cout << "FACET field not valid\n";
	}

	if (data & BYTE_VERIF_AVERAGING) {
		std::cout << "AVERAGING field not valid\n";
	}

	return true;
}
