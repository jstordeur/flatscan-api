#include "serial.h"

#include <array>
#include <string.h>
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()

speed_t to_speed_t(int baudrate) {
    switch(baudrate) {
        case 57600: return B57600;
        case 115200: return B115200;
        case 230400: return B230400;
        case 460800: return B460800;
        case 500000: return B500000;
        case 576000: return B576000;
        case 921600: return B921600;
        default: return B115200;
    }
}

Serial::Serial() { }

Serial::Serial(std::string path, int baudrate) {
    this->open(path, baudrate);
}

bool Serial::open(std::string path, int baudrate) {
    fd_ = ::open(path.c_str(), O_RDWR);

    if (fd_ < 0) {
        printf("Error %i from open: %s\n", errno, strerror(errno));
    }

    struct termios tty;

    /* read actual state */
    if(tcgetattr(fd_, &tty) != 0) {
        printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
    }

    /* set parameters */
    tty.c_cflag &= ~PARENB;         /** Disable Parity */
    tty.c_cflag &= ~CSTOPB;         /** One stop bit */
    tty.c_cflag &= ~CSIZE;          /** 8 bits per byte (most common) */
    tty.c_cflag |= CS8;
    tty.c_cflag &= ~CRTSCTS;        /** Disable flow control */
    tty.c_cflag |= CREAD | CLOCAL;

    tty.c_lflag &= ~ICANON;
    tty.c_lflag &= ~ECHO;           /** Disable echo */
    tty.c_lflag &= ~ECHOE;          /** Disable erasure */
    tty.c_lflag &= ~ECHONL;         /** Disable new-line echo */
    tty.c_lflag &= ~ISIG;           /** Disable interpretation of INTR, QUIT and SUSP */

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); /** Turn off s/w flow ctrl */
    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); /** Disable any special handling of received bytes */

    tty.c_oflag &= ~OPOST;          /** Prevent special interpretation of output bytes (e.g. newline chars) */
    tty.c_oflag &= ~ONLCR;          /** Prevent conversion of newline to carriage return/line feed */

    tty.c_cc[VTIME] = 0;            /** Unblocking read */
    tty.c_cc[VMIN] = 0;    

    cfsetispeed(&tty, to_speed_t(baudrate));     /** baudrate */
    cfsetospeed(&tty, to_speed_t(baudrate));     /** baudrate */

    if (tcsetattr(fd_, TCSANOW, &tty) != 0) {
        printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
    }

    return fd_;
}

bool Serial::isOpen() {
    return is_open_;
}

ssize_t Serial::write(const std::vector<uint8_t> buffer) {
    int n = ::write(fd_, buffer.data(), buffer.size());
    return n;
}

ssize_t Serial::read(std::vector<uint8_t> buffer, size_t size) {
    uint8_t read_buf[256];
    int n = ::read(fd_, &read_buf, size);
    buffer.assign(read_buf, read_buf+n);
    return n;
}