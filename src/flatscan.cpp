#include "flatscan.h"
#include "serial.h"
#include <execinfo.h>
#include <signal.h>

using namespace std::chrono;

std::ostream& operator<<(std::ostream& os, const Flatscan& fs) {
	os << fs.parameters_ << std::endl;

	os << "[MDI information]\n";
	os << "  MDI transfer mode : " << (fs.mdi_mode_ == SINGLE_SHOT ? "single shot" : "continuous") << std::endl;
	if (fs.mdi_mode_ == CONTINUOUS)
		os << "  Frequency : " << fs.frequency() << " hz\n";
	os << "  MDI header size : " << fs.mdi_header_size_ << std::endl;
	os << "  Angle step : " << fs.angle_step_ << " deg";
	return os;
}

Flatscan::Flatscan(std::string device, int baudrate)
{
	Serial* ser;

	try {
		ser = new Serial(device, baudrate);
	}
	catch(std::exception &e) {
		throw std::string(e.what());
	}

	if(!ser->isOpen()) {
		throw std::string("Cannot open file descriptor");
	}

	decoder_ = new Decoder(*ser);
	encoder_ = new Encoder(*ser);

	cached_frame_.set_capacity(5);

	mdi_mode_ = CONTINUOUS;

	if (init() == false) {
		throw std::string("Init timeout");
	}
}

bool Flatscan::init(unsigned int nb_try) {
	for (unsigned int j = 0; j < nb_try; j++) {
		if (get_parameters(parameters_)) {
			update_config();
			return true;
		}
	}

	return false;
}

bool Flatscan::measurments(std::vector<Pol>& p) {
	std::vector<Pol> vec;

	if (decoder_->has_data()) {
		Frame f = decoder_->frame();

		/**
		 * If it's not a MDI frame save it in cached_frame_
		 * because the queue is poped in Decoder class
		**/
		if (f.cmd_ != Frame::Cmd_fth::MDI) {
			cached_frame_.push_back(f);

			return false;
		}

#ifdef OLD_FIRMWARE
		static bool unsetted = true;
		// Set CAN number
		if (unsetted) {
			can_ = 0;
			for (int i = 0; i < CAN_SIZE; ++i) {
				can_ |= f.data_[i] << (i * 8);
			}

			unsetted = false;
		}
#endif
		// Delete unwanted header
		f.data_.erase(f.data_.begin(), f.data_.begin() + mdi_header_size_);

		// Compute remission
		float rho, theta = - angle_step_ / 2;
		int remission;

		// Compute until variable
		size_t until = f.data_.size();
		size_t remi_shift = 0;
		if (parameters_.mdi() == Mdi::MDI_DISTREMI) {
			remi_shift = until = f.data_.size() / 2;
		}

		// loop trough data
		for (size_t i = 0; i < until; i += 2) {
			theta += angle_step_;
			rho = parameters_.mdi() == Mdi::MDI_REMI ?
				0 :
				f.data_[i] | f.data_[i+1] << 8;

			remission = parameters_.mdi() == Mdi::MDI_DIST ?
				0 :
				f.data_[i + remi_shift] | f.data_[i+1 + remi_shift] << 8;

			vec.push_back({rho, theta, remission});
		}

		// Assign vector
		p = vec;

		return true;
	}
	else {
		return false;
	}
}

bool Flatscan::measurments(std::vector<Rect>& r) {
	std::vector<Pol> p;

	if (measurments(p)) {
		r.assign(p.size(), Rect());

		for (size_t i = 0; i < p.size(); ++i) {
			r[i].x = p[i].rho * std::cos(p[i].theta * TO_RAD);
			r[i].y = p[i].rho * std::sin(p[i].theta * TO_RAD);
			r[i].remission = p[i].remission;
		}
		return true;
	}
	else {
		return false;
	}
}

bool Flatscan::set_mdi_transfer_mode(MDI_transfer_mode mode) {
	get_measurments(mode);

	return true;
}

Parameters Flatscan::parameters() {
	return parameters_;
}

bool Flatscan::set_parameters(Parameters& p) {
	// Send frame
	try {
		*encoder_ << Frame::Cmd_htf::SET_PARAMETERS << p.to_vector() << Encoder::CMD::SEND;
	}
	catch (std::string &e) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] Nothing was sent\n";
		std::cout << e << std::endl;
		return false;
	}

	// Search ack frame
	Frame f_in = search_frame(Frame::Cmd_fth::SEND_PARAMETERS, NUMBER_FRAME_SEARCH);

	if (f_in.cmd_ == EMPTY_FRAME) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] No corresponding frame found\n";
		return false;
	}

	// Data
	uint32_t verif = f_in.data_[0] | f_in.data_[1] << 8 | f_in.data_[2] << 16 | f_in.data_[2] << 24;

	if (Parameters::ill_formed(verif)) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] Parameters set are ill formed\n";
		return false;
	}

	parameters_.set_data(f_in.data_);
	p = parameters_;
	update_config();

	return true;
}

bool Flatscan::get_parameters(Parameters& p) {
	// Send frame
	try {
		*encoder_ << Frame::Cmd_htf::GET_PARAMETERS << Encoder::CMD::SEND;
	}
	catch (std::string &e) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] Nothing was sent\n";
		std::cout << e << std::endl;
		return false;
	}

	// Search ack frame
	Frame f_in = search_frame(Frame::Cmd_fth::SEND_PARAMETERS, NUMBER_FRAME_SEARCH);

	if (f_in.cmd_ == EMPTY_FRAME) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] No corresponding frame found\n";

		return false;
	}

	// Data
	parameters_.set_data(f_in.data_);
	p = parameters_;
	update_config();

	return true;
}

uint32_t Flatscan::get_can() {
#ifdef OLD_FIRMWARE
	std::vector<Pol> p;
	while(!measurments(p)) {
		usleep(100);
	}
	return can_;
#else
	// Send frame
	try {
		*encoder_ << Frame::Cmd_htf::GET_IDENTITY << Encoder::CMD::SEND;
	}
	catch (std::string &e) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] Nothing was sent\n";
		std::cout << e << std::endl;
		return false;
	}

	// Search ack frame
	Frame f_in = search_frame(Frame::Cmd_fth::SEND_IDENTITY, NUMBER_FRAME_SEARCH);

	if (f_in.cmd_ == EMPTY_FRAME) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] No corresponding frame found\n";

		return false;
	}

	// Data
	uint32_t can = 0;
	for (int i = 0; i < CAN_SIZE; ++i) {
		can |= f_in.data_[i + CAN_POS] << (i * 8);
	}

	return can;
#endif
}

bool Flatscan::get_measurments(MDI_transfer_mode mode) {
	// Send frame
	try {
		*encoder_ << Frame::Cmd_htf::GET_MEASURMENTS << mode << Encoder::CMD::SEND;
	}
	catch (std::string &e) {
		std::cout << "[" << __PRETTY_FUNCTION__ << "] Nothing was sent\n";
		std::cout << e << std::endl;
		return false;
	}

	// Received frame retreive with measurments()

	// Data
	mdi_mode_ = mode;

	decoder_->reset_frequency();

	return false;
}

Frame Flatscan::cached_frame() {
	Frame f;

	if (has_cached()) {
		f = cached_frame_.front();

		cached_frame_.pop_front();
	}

	return f;
}

bool Flatscan::has_cached() {
	return !cached_frame_.empty();
}

bool Flatscan::flush_cached() {
	cached_frame_.clear();

	return false;
}

Frame Flatscan::search_frame(uint16_t cmd, unsigned int nb_try) {
	auto start = high_resolution_clock::now();
	auto now = high_resolution_clock::now();

	Frame f;

	unsigned int j = 0;
	while(j < nb_try && duration_cast<milliseconds>(now - start).count() < SEARCH_FRAME_TIMEOUT) {
		if (decoder_->has_data()) {
			f = decoder_->frame();

			#ifdef DEBUG_FLATSCAN
			std::cout << "[" << __PRETTY_FUNCTION__ << "] got_something : " << std::hex << std::uppercase << (int)f.cmd_ << std::endl << std::dec;
			std::cout << "[" << __PRETTY_FUNCTION__ << "] Search iteration : " << j << std::endl;
			#endif

			if (f.cmd_ == cmd) {
				return f;
			}
			else {
				cached_frame_.push_back(f);
			}

			j++;
		}

		now = high_resolution_clock::now();
	}

	// Search in cached frame if failed
	while(j < nb_try && duration_cast<milliseconds>(now - start).count() < SEARCH_FRAME_TIMEOUT) {
		if (has_cached()) {
			f = cached_frame();

			#ifdef DEBUG_FLATSCAN
			std::cout << "[" << __PRETTY_FUNCTION__ << "] got_something : " << std::hex << std::uppercase << (int)f.cmd_ << std::endl << std::dec;
			std::cout << "[" << __PRETTY_FUNCTION__ << "] Search iteration : " << j << std::endl;
			#endif

			if (f.cmd_ == cmd) {
				return f;
			}

			j++;
		}

		now = high_resolution_clock::now();
	}

	flush_cached();

	// Return EMPTY_FRAME if nothing found
	return Frame();
}

void Flatscan::update_config() {
	mdi_header_size_ =
		(CAN_SIZE + CNTR_SIZE) * parameters_.can_cntr() +
		CTN_SIZE * parameters_.ctn() +
		FACET_SIZE * parameters_.facet();

	angle_step_ = (parameters_.angle()[1] - parameters_.angle()[0]) / (float)parameters_.number_spot();

	decoder_->reset_frequency();
}

int Flatscan::frequency() const {
	if (mdi_mode_ == CONTINUOUS)
		return decoder_->frequency_;
	else
		return 0;
}
