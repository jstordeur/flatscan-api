#include "encoder.h"

Encoder::Encoder(Serial& ser) :
ser_(ser)
{

}

bool Encoder::send(const Frame& f) {
	const std::vector<uint8_t> data = f.pack();

	size_t number = ser_.write(data);

	if (number != data.size()) {
		std::ostringstream e;
		e << "[" << __PRETTY_FUNCTION__ << "] Write " << number << " of " << data.size() << " bytes";
		throw e.str();
	}

	return number == data.size();
}

Encoder& Encoder::operator<<(Frame::Cmd_htf const &rhs) {
	frame_.prefill();

	frame_.cmd_ = rhs;

	frame_.sync_[BYTE_SIZE] = (SYNC_SIZE + CMD_SIZE + CHK_SIZE) & 0xFF;
	frame_.sync_[BYTE_SIZE + 1] = ((SYNC_SIZE + CMD_SIZE + CHK_SIZE) >> 8) & 0xFF;

	frame_.chk_ = frame_.compute_crc16();
	return *this;
}

Encoder& Encoder::operator<<(uint8_t const &rhs) {
	std::vector<uint8_t> vec({rhs});

	*this << vec;

	return *this;
}

Encoder& Encoder::operator<<(std::vector<uint8_t> const &rhs) {
	frame_.data_ = rhs;

	frame_.sync_[BYTE_SIZE] += (frame_.data_.size()) & 0xFF;
	frame_.sync_[BYTE_SIZE + 1] += (frame_.data_.size() >> 8) & 0xFF;

	frame_.chk_ = frame_.compute_crc16();

	return *this;
}

Encoder& Encoder::operator<<(Encoder::CMD const &rhs) {
	if (rhs == SEND) send(frame_);

	frame_ = Frame();

	return *this;
}
