#include "frame.h"

Frame::Frame() {
	cmd_ = EMPTY_FRAME;
}

std::ostream& operator<<(std::ostream& os, const Frame& f) {
	os << "Frame :\n---\n";

	os << "SYNC :\n";
	for (auto it : f.sync_) {
		os << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)it << " " << std::dec;
	} os << std::endl << "---\n";

	os << "CMD :\n";
	os << std::hex << std::uppercase << (int)(f.cmd_) << std::endl << std::dec;
	switch (f.cmd_) {
		case Frame::Cmd_fth::MDI :
			os << "MDI frame\n";
		break;

		case Frame::Cmd_fth::SEND_IDENTITY :
			os << "Send identity frame\n";
		break;

		case Frame::Cmd_fth::SEND_PARAMETERS :
			os << "Send parameters frame\n";
		break;

		case Frame::Cmd_fth::EMERGENCY :
			os << "Emergency frame\n";
		break;
	} os << "---\n";

	os << "DATA : \n" << f.data_.size() << " bytes\n";
	for (auto it : f.data_) {
		os << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)it << " " << std::dec;
	} os << std::endl << "---\n";

	os << "CHK :\n";
	os << std::hex << std::uppercase << (int)(f.chk_) << std::dec;

	return os;
}

uint16_t Frame::compute_crc16(std::vector<uint8_t> buf) {
	uint16_t crc = 0;

	for (uint16_t i = 0; i < buf.size(); ++i) {
		crc ^= (uint16_t)(buf[i] << 8);

		for (uint16_t j = 0; j < 8; ++j) {
			if ((crc & 0x8000) != 0) {
				crc = (uint16_t)((crc << 1) ^ BEA_POLYNOM);
			}
			else {
				crc <<=1;
			}
		}
	}

	/*std::vector<uint8_t> vec;

	vec.push_back(crc & 0xFF);
	vec.push_back((crc >> 8) & 0xFF);*/

	return crc;
}

uint16_t Frame::compute_crc16() {
	std::vector<uint8_t> buf;

	buf.insert(buf.end(), sync_.begin(), sync_.end());
	buf.push_back(cmd_ & 0xFF);
	buf.push_back( (cmd_ >> 8) & 0xFF );
	buf.insert(buf.end(), data_.begin(), data_.end());

	return compute_crc16(buf);
}

std::vector<uint8_t> Frame::pack() const {
	std::vector<uint8_t> buf;

	buf.insert(buf.end(), sync_.begin(), sync_.end());

	buf.push_back(cmd_ & 0xFF);
	buf.push_back( (cmd_ >> 8) & 0xFF );

	buf.insert(buf.end(), data_.begin(), data_.end());

	buf.push_back(chk_ & 0xFF);
	buf.push_back( (chk_ >> 8) & 0xFF );

	return buf;
}

void Frame::unpack(std::vector<uint8_t> buffer) {
	auto cursor = buffer.begin();

	// Parse SYNC
	std::copy_n(cursor, SYNC_SIZE, sync_.begin());
	cursor += SYNC_SIZE;

	// PARSE CMD
	cmd_ = static_cast<uint16_t>(*(cursor) | *(cursor+1) << 8);
	cursor += CMD_SIZE;

	// PARSE DATA
	data_.assign(cursor, buffer.end() - CHK_SIZE);
	cursor = buffer.end() - CHK_SIZE;

	// PARSE CHK
	chk_ = *cursor | *(cursor + 1) << 8;
}

void Frame::prefill() {
	std::vector<uint8_t> pattern(BEA_PATTERN);

	std::copy_n(pattern.begin(), 4, sync_.begin());
	sync_[4] = 0x02;
	// 5-6 are size
	sync_[7] = 0x02;
	sync_[8] = 0;
	sync_[9] = 0;
	sync_[10] = 0;
}
