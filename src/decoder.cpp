#include "decoder.h"

using namespace std::chrono;

Decoder::Decoder(Serial& ser, size_t max_size) :
ser_(ser),
sync_pattern_(BEA_PATTERN),
error_(FINE),
frequency_(0)
{
	if (max_size <= 0)
		max_size_ = 5;
	else
		max_size_ = max_size;

	frame_.set_capacity(max_size_);

	reset_frequency();

	// Launch sniffer thread
	boost::thread t{boost::bind(&Decoder::run, this)};
}

Frame Decoder::frame() {
	std::lock_guard<std::mutex> guard(container_mut_);
	Frame f;

	if (frame_.empty() == false) {
		f = frame_.front();

		frame_.pop_front();
	}

	return f;
}

bool Decoder::error(Decoder::Error& e) {
	if (error_) {
		e = error_;
		error_ = FINE;

		return true;
	}
	else {
		e = FINE;

		return false;
	}
}

bool Decoder::has_data() {
	std::lock_guard<std::mutex> guard(container_mut_);
	return !frame_.empty();
}

bool Decoder::flush() {
	std::lock_guard<std::mutex> guard(container_mut_);
	frame_.clear();

	return true;
}

void Decoder::run() {
	uint16_t raw_frame_size;
	bool synchronised = false;

	std::vector<uint8_t> buffer;
	std::vector<uint8_t>::iterator byte;
	uint16_t crc_frame, crc_compute;

	while(1) {

		// No need to go further if nothing is read
		if (ser_.read(buffer, 1) != 1) {
			// Third of max frequency
			usleep(30000);
			continue;
		}

		if (synchronised == false) {
			// Search for pattern in buffer
			byte = std::search(buffer.begin(), buffer.end(), sync_pattern_.begin(), sync_pattern_.end());

			// Pattern found
			if (byte != buffer.end()) {

				if (byte != buffer.begin()) {
					error_ = DESYNCHRONIZE;
				}

				// Erase previous incomplete frame
				buffer.erase(buffer.begin(), byte);

				// Decoder is now synchronised
				synchronised = true;
			}
		}
		else {
			if (buffer.size() < BYTE_SIZE + 2) {
				continue;
			}
			else if (buffer.size() == BYTE_SIZE + 2) {
				raw_frame_size = buffer[BYTE_SIZE] | buffer[BYTE_SIZE + 1] << 8;
			}
			else if (buffer.size() == raw_frame_size) {

				// Verify crc16
				crc_compute = Frame::compute_crc16(std::vector<uint8_t>(buffer.begin(), buffer.end() - 2));

				// **r**begin so the weight is inversed
				crc_frame = *(buffer.rbegin()) << 8 | *(buffer.rbegin() + 1);

				if (crc_frame != crc_compute) {
					error_ = CHKSUM_DO_NOT_MATCH;

					buffer.clear();
					synchronised = false;

					continue;
				}

				#ifdef DEBUG_DECODER
					std::cout << "[" << __PRETTY_FUNCTION__ << "] Received " << buffer.size() << " bytes:\n";
					for (auto it : buffer) {
						std::cout << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << (int)it << " " << std::dec;
					} std::cout << std::endl << "---\n";
				#endif

				queued(buffer);
				buffer.clear();

				synchronised = false;
			}
			else if (buffer.size() > raw_frame_size){
				error_ = SIZE_DO_NOT_MATCH;

				buffer.clear();
				synchronised = false;
			}
		}
	}
}

void Decoder::queued(std::vector<uint8_t> buffer) {
	std::lock_guard<std::mutex> guard(container_mut_);

	Frame f;

	f.unpack(buffer);

	compute_frequency(f);

	frame_.push_back(f);
}

bool Decoder::compute_frequency(Frame f) {
	if (f.cmd_ == Frame::Cmd_fth::MDI) {
		frame_cntr_++;
		auto duration = duration_cast<microseconds>(high_resolution_clock::now() - start_);

		frequency_ = frame_cntr_ / (duration.count() / 1000000.0);

		return true;
	}

	return false;
}

bool Decoder::reset_frequency() {
	frame_cntr_ = 0;
	start_ = high_resolution_clock::now();

	return true;
}
