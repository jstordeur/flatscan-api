#ifndef __ENCODER__
#define __ENCODER__

#include <sstream>

#include "serial.h"
#include "frame.h"

//#define DEBUG_ENCODER

class Encoder {
public :
	/**
	 * @brief command use to terminate a frame.
	 * SEND send the frame before flushing the cached frame
	 * FLUSH flushe the cached frame
	 */
	enum CMD {
		SEND,
		FLUSH
	};

	Encoder(Serial& ser);

	/**
	 * @brief Overload operator<< to set the command of the cached frame
	 * 
	 * @param rhs is the command of type Frame::Cmd_htf
	 * @return Encoder& to chain the operator<<
	 */
	Encoder& operator<<(Frame::Cmd_htf const &rhs);

	/**
	 * @brief Overload operator<< to set data of 1 byte to the cached frame
	 * 
	 * @param rhs is the data as uint8_t to be send
	 * @return Encoder& to chain the operator<<
	 */
	Encoder& operator<<(uint8_t const &rhs);

	/**
	 * @brief Overlaod the operator<< to set data of mutliple bytes to the cached frame
	 *
	 * @param rhs is a vector of data to be send
	 * @return Encoder& to chain the operator<<
	 */
	Encoder& operator<<(std::vector<uint8_t> const &rhs);

	/**
	 * @brief Overload the operator<< to set terminate the editing if the cached frame
	 * 
	 * @param rhs is a vector of data to be send
	 * @return Encoder& to chain the operator<<
	 */
	Encoder& operator<<(CMD const &rhs);
private :
	Serial& ser_;

	Frame frame_;

	bool send(const Frame& f);

friend class Flatscan;
};

#endif
