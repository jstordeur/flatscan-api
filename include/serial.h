#ifndef __SERIAL__
#define __SERIAL__

#include <string>
#include <vector>

class Serial {
public:
    Serial();
    Serial(std::string path, int baudrate);

    bool open(std::string path, int baudrate);
    bool isOpen();

    ssize_t write(const std::vector<uint8_t> buffer);
    ssize_t read(std::vector<uint8_t> buffer, size_t size);

protected:

private:
    int fd_;
    bool is_open_;
};

#endif