#ifndef __FRAME__
#define __FRAME__

#include <iostream>		// to_string
#include <iomanip>		// to_string
#include <vector>		// frame container
#include <array>		// frame container
#include <algorithm>	// copy_n

#include "parameters.h"

//#define DEBUG_FRAME

#define SYNC_SIZE	11
#define CMD_SIZE	2
#define CHK_SIZE	2

#define BYTE_SIZE 5

/**
 * @def BEA_POLYNOM
 * @brief Polynom used to compute the crc16 of a frame
 */
#define BEA_POLYNOM 0x90d9

/**
 * @def BEA_PATTERN
 * @brief Pattern to sync with incomming frame
 */
#define BEA_PATTERN {0xBE, 0xA0, 0x12, 0x34}

#define EMPTY_FRAME 0xAB'CD

/**
 * @class Frame
 * @brief Frame class is used to store any kind of frame by unpacking them and associate the right command enum to match the right type of frame
 */
class Frame {
public:
	/**
	 * @enum Cmd_htf
	 * @brief Used to tag a frame from host to flatscan
	 */
	enum Cmd_htf {
		// Host to flatscan
		SET_BAUDRATE = 0xC3'51,
		GET_MEASURMENTS = 0xC3'5B,
		GET_IDENTITY = 0xC3'5A,
		GET_EMERGENCY = 0xC3'6E,
		GET_PARAMETERS = 0xC3'54,
		SET_PARAMETERS = 0xC3'53,
		STORE_PARAMETERS = 0xC3'55,
		RESET_MDI_COUNTER = 0xC3'5E,
		RESET_HEARTBEAT_COUNTER = 0xC3'5F,
		RESET_EMERGENCY_COUNTER = 0xC3'61,
		SET_LED = 0xC3'78
	};

	/**
	 * @enum Cmd_fth
	 * @brief Used to tag a frame from flatscan to host
	 */
	enum Cmd_fth {
		// Flatscan to host
		MDI = 0xC3'5B,
		SEND_IDENTITY = 0xC3'5A,
		SEND_PARAMETERS = 0xC3'54,

		HEARTBEAT = 0xC3'64,
		EMERGENCY = 0xC3'6E
	};

	/**
	 * @fn Frame()
	 * @brief basic constructor
	 */
	Frame();

	/**
	 * @fn std::vector<uint8_t> compute_crc16(std::vector<uint8_t> buf)
	 * @static
	 * @brief Compute crc16 on a vector of byte with the help of the BEA_POLYNOM
	 * @return Return the crc16 as a vector of two bytes.
	 */
	static uint16_t compute_crc16(std::vector<uint8_t> buf);

	/**
	 * @brief Compute crc16 on the current frames
	 * 
	 * @return uint16_t 
	 */
	uint16_t compute_crc16();

	/**
	 * @brief Overload operator<< to dump the frame state
	 */
	friend std::ostream& operator<<(std::ostream& os, const Frame& f);

private:
	std::array<uint8_t, SYNC_SIZE> sync_;
	uint16_t cmd_;
	std::vector<uint8_t> data_;
	uint16_t chk_;

	std::vector<uint8_t> pack() const;
	void unpack(std::vector<uint8_t> buffer);
	
	bool empty() {return cmd_ == EMPTY_FRAME;}
	void prefill();

friend class Decoder;
friend class Encoder;
friend class Flatscan;
};

#endif
