#ifndef __PARAMETER__
#define __PARAMETER__

#include <iostream>
#include <iomanip>
#include <vector>
#include <array>
#include <cassert>
#include <algorithm>

//#define DEBUG_PARAMETERS

#define PARAMETERS_SIZE 22

#define BYTE_CTN 1
#define BYTE_MDI 2
#define BYTE_ACQUISITION 3
#define BYTE_OPTIMISATION 4
#define BYTE_SPOT 8
#define BYTE_ANGLE_FIRST 14
#define BYTE_ANGLE_LAST 16
#define BYTE_CAN_CNTR 18
#define BYTE_FACET 20
#define BYTE_AVERAGING 21

/**
 * @brief Header size of SEND_PARAMETERS frame
 * - 4 bytes for the conformity of parameters sent
 * - 2 for future use
 */
#define BYTE_HEADER_DATA 6

#define BYTE_VERIF_CTN 1 << 1
#define BYTE_VERIF_MDI 1 << 2
#define BYTE_VERIF_ACQUISITION 1 << 3
#define BYTE_VERIF_OPTIMISATION 1 << 4
#define BYTE_VERIF_SPOT 1 << 9
#define BYTE_VERIF_ANGLE_FIRST 1 << 12
#define BYTE_VERIF_ANGLE_LAST 1 << 13
#define BYTE_VERIF_CAN_CNTR 1 << 14
#define BYTE_VERIF_HEARTBEAT 1 << 15
#define BYTE_VERIF_FACET 1 << 16
#define BYTE_VERIF_AVERAGING 1 << 17

/**
 * @brief enumeration to match the datasheet
 */
enum State {
	DISABLE = 0,
	ENABLE = 1
};

/**
 * @brief enumeration to match the datasheet
 */
enum Mdi {
	MDI_DIST = 0,
	MDI_REMI = 1,
	MDI_DISTREMI = 2
};

/**
 * @brief enumeration to match the datasheet
 */
enum Acquisition {
	HS = 0,
	HD = 1
};

/**
 * @brief enumeration to match the datasheet
 */
enum Optimisation {
	OPTI_DISBALE = 0,
	OPTI_2_5 = 1,
	OPTI_3_0 = 2,
	OPTI_3_5 = 3,
	OPTI_MAX = 4
};

/**
 * @brief enumeration to match the datasheet
 */
enum Averaging {
	AVRG_DISABLE = 0,
	AVRG_3PTS = 1,
	AVRG_3PTS_NEIGHBOURS = 2,
	AVRG_5PTS = 3,
	AVRG_5PTS_NEIGHBOURS = 4
};

/**
 * @brief Class used to manipulate the data array representing the parameters
 * 
 */
class Parameters {
public:
	Parameters();

	State ctn() const;
	Mdi mdi() const;
	Acquisition acquisition() const;
	Optimisation optimisation() const;
	uint16_t number_spot() const;

	/**
	 * @brief get the angle minimum and maximum in degree
	 * 
	 * @return std::array<uint16_t, 2> where the first case is the minimum and the second the maximum angle
	 */
	std::array<uint16_t, 2> angle() const;
	State can_cntr() const;
	State facet() const;
	Averaging averaging() const;
	float scan_time() const;
	std::array<uint8_t, PARAMETERS_SIZE> data() const;
	std::vector<uint8_t> to_vector() const;

	bool set_ctn(State state);
	bool set_mdi(Mdi mode);
	bool set_acquisition(Acquisition mode);
	bool set_optimisation(Optimisation mode);
	/**
	 * @brief Set the number of spot
	 * The number must match the datasheet indiquation
	 * 
	 * @param number 
	 * @return false if any errors occurs
	 */
	bool set_number_spot(uint16_t number);
	/**
	 * @brief Set the angle minimum and maximum in degree
	 * 
	 * @param min angle in degree
	 * @param max angle in degree
	 * @return false if any errors occurs
	 */
	bool set_angle(uint16_t min, uint16_t max);
	bool set_can_cntr(State state);
	bool set_facet(State state);
	bool set_averaging(Averaging mode);
	bool set_data(std::vector<uint8_t> vec);
	bool set_data(std::array<uint8_t, PARAMETERS_SIZE> arr);

	/**
	 * @brief Verify if the parameters is ill formed.
	 * Used with the raw received data from Flatscan::set_parameters()
	 * 
	 * @param data the concatenation of the 4 first bytes of SEND_PARAMETERS frame
	 * @return false if any errors occurs
	 */
	static bool ill_formed(const uint32_t& data);

	/**
	 * @brief Overload of operator<< to dump the information of the object
	 */
	friend std::ostream& operator<<(std::ostream& os, const Parameters& p);

private:
	std::array<uint8_t, PARAMETERS_SIZE> data_;
};

#endif
