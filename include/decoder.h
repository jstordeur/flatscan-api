#ifndef __DECODER__
#define __DECODER__

#include <iostream>			// debug
#include <iomanip>			// debug
#include <vector>			// buffer
#include <queue>			// frame
#include <chrono>			// frequency
#include <algorithm>		// std::search
#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>
#include <mutex>

#include "serial.h"
#include "frame.h"

//#define DEBUG_DECODER

/**
 * @class Decoder
 * @brief Class handling the reading of the serial port and converting received raw bytes frame into an unpacked Frame.
 */
class Decoder {
public:
	/**
	 * @enum Error
	 * @brief Enumeration of all type of errors that the decoders can handle
	 */
	enum Error {
		FINE = 0,
		CHKSUM_DO_NOT_MATCH,
		SIZE_DO_NOT_MATCH,
		DESYNCHRONIZE
	};

	/**
	 * @fn Decoder (serial::Serial & ser, size_t max_size = 5)
	 * @brief Class constructor.
	 *
	 * @param As first parameter, a serial::Serial object used to read on the serial.
	 *        The second is optionnal and set size of the queue container.
	 */
	Decoder(Serial& ser, size_t max_size = 5);

	/**
	 * @fn void run (void)
	 * @brief Main loop function. Threaded !
	 *        Used to read endlessly the serial port without interrupting anything.
	 *        1. It waits to match the synchronize Pattern;
	 *        2. Then it waits to get the size of the frame (bytes 5 and 6 of sync field);
	 *        3. Wait for the whole frame to be reiceived;
	 *        4. Verify the integrity of the message with a crc16;
	 *        5. unpack into a Frame frame then queued into the container. Erase the last frame if the container is greater than max_size.
	**/
	void run();

	/**
	 * @fn Frame frame (void)
	 * @brief Dequed FIFO stored frame.
	 * @return Return the last queued frame (FIFO buffer) as a Frame class.
	 *         If the queue is empty the behaviour is undefined.
	 *         Call has_data() to know if a frame is stored.
	 */
	Frame frame();

	/**
	 * @fn bool error(Error & e)
	 * @brief Write into e the actual error state of the decoder.
	 *        When it's done, the error is erased and reinitialized to Error::FINE
	 * @return Boolean if error exist or not
	 */
	bool error(Error& e);

	/**
	 * @fn bool has_data (void)
	 * @brief Get the information if the frame container has data
	 * @return Return a bool. True if the container is not empty.
	 */
	bool has_data();

	/**
	 * @brief flush the frame_ queue
	 * 
	 * @return true if data has been erased
	 */
	bool flush();

	bool reset_frequency();
private:
	Serial& ser_;

	/**
	 * @brief boost version of queue to keep in memory the last frame reiceived.
	 *        Push by queued()
	 *        the size is limited and the pushed element pop the oldest if the size become too big
	 */
	boost::circular_buffer_space_optimized<Frame> frame_;
	std::mutex container_mut_;


	std::vector<uint8_t> sync_pattern_;

	Error error_;
	size_t max_size_;

	std::chrono::high_resolution_clock::time_point start_;
	unsigned long frame_cntr_;
	int frequency_;
	
	void queued(std::vector<uint8_t> buffer);

	bool compute_frequency(Frame f);

friend class Flatscan;
};

#endif
