#ifndef __FLATSCAN__
#define __FLATSCAN__

#include <queue>
#include <chrono>
#include <future>
#include <boost/circular_buffer.hpp>

#include "parameters.h"
#include "frame.h"
#include "decoder.h"
#include "encoder.h"

//#define DEBUG_FLATSCAN

/**
 * @brief uncomment if the Flatscan is flashed with the old firmware
 */
#define OLD_FIRMWARE

/**
 * @brief Represent of MDI frame are sent
 */
enum MDI_transfer_mode {
	SINGLE_SHOT = 0,
	CONTINUOUS = 1
};

#define TO_DEG 57.295779513
#define TO_RAD 0.017453293

/**
 * @brief Use to compute the header of MDI frame
 * 
 */
#define CAN_SIZE 4
#define CNTR_SIZE 2
#define CTN_SIZE 2
#define FACET_SIZE 1

/**
 * @brief Use to search our frame in the queue of decoder_ and in cached_frame_
 * 
 */
#define NUMBER_FRAME_SEARCH 20
#define SEARCH_FRAME_TIMEOUT 500

#define CAN_POS 7

/**
 * @struct Pol
 * @brief Used to store measurments as polar
 */
struct Pol {
	/**
	 * @brief Store the distance in meters
	 */
	float rho;

	/**
	 * @brief Store the angle in radians
	 */
	float theta;

	/**
	 * @brief Store the remission value
	 */
	int remission;
};

/**
 * \struct Rect
 * \brief Used to store measurments as rectangular
 */
struct Rect {
	/**
	 * @brief Store the distance in meters in the x axis
	 */
	float x;
	/**
	 * @brief Store the distance in meters in the y axis
	 */
	float y;

		/**
	 * @brief Store the remission value
	 */
	int remission;
};

/**
 * @class Flatscan
 * @brief Main class representing a Flatscan
 */
class Flatscan {
public:
	/**
	 * @fn Flatscan(std::string device, int baudrate = 921600)
	 * @brief Basic constructor
	 * @throw Throw a std::string if something goes wrong
	 * @param device is the serial port
	 * @param baudrate is the desired baudrate. The default one is set to 921600
	 */
	Flatscan(std::string device, int baudrate = 921600);

	// Functions that will send or read frame to/from the Flatscan

	/**
	 * @brief Set the parameters object by **sending a frame** to the Flatscan
	 * 
	 * @param p Which is a reference to a Parameters object
	 * @return false if any errors occurs
	 */
	bool set_parameters(Parameters& p);

	/**
	 * @brief Get the parameters object by **sending a frame** to the Flatscan
	 * 
	 * @param p Which is a reference to a Parameters object
	 * @return false if any errors occurse 
	 */
	bool get_parameters(Parameters& p);

	uint32_t get_can();

	/**
	 * @brief Send a get_measurments frame to the flatscan.
	 *        Usefull when in SINGLE_SHOT mode where we need to ask for a frame before calling Flatscan::measurments();
	 *        To retreive the actual measurment call Flatscan::measurments()
	 * 
	 * @param mode Which is how the Flatscan will send the MDI frames. Can be in continuous or in single shot
	 * @return false if any errors occurse
	 */
	bool get_measurments(MDI_transfer_mode mode = SINGLE_SHOT);

	// Higher level functions

	/**
	 * @fn bool get_measurment(std::vector<Pol>& p)
	 * @brief Ask Decoder the last FIFO frame and extract the measurments.
	 *        If the poped value is not a MDI frame it will be cached and will be retrievable with cached_frame()
	 *        The measurments are in polar coordinates
	 *        The measurments are stored in concordance of Mdi parameter
	 * @param p which is a reference to a vector of Pol
	 * @return **True** if a measurment was writed to the  vector given as parameter
	 */
	bool measurments(std::vector<Pol>& p);

	/**
	 * @fn bool get_measurment(std::vector<Rect>& r)
	 * @brief Ask Decoder the last FIFO frame and extract the measurments.
	 *        If the poped value is not a MDI frame it will be cached and will be retrievable with cached_frame()
	 *        The measurments are in rectangular coordinates
	 * @param r which is a reference to a vector of Pol
	 * @return **True** if a measurment was writed to the  vector given as parameter
	 */
	bool measurments(std::vector<Rect>& r);

	/**
	 * @brief Set the mdi transfer mode. Can be continuous or single shot
	 * 
	 * @param mode Which is how the Flatscan will send the MDI frames. Can be in continuous or in single shot
	 * @return false if any errors occurs
	 */
	bool set_mdi_transfer_mode(MDI_transfer_mode mode);

	/**
	 * @fn Frame cached_frame()
	 * @brief Get the last cached frame.
	 *        A cached frame is queued when a get_measurment is asked and the type is not a MDI frame
	 */
	Frame cached_frame();

	/**
	 * @fn bool has_cached()
	 * @brief Let now if the cached frame is not empty
	 * @return Return true if the container is not empty
	 */
	bool has_cached();

	/**
	 * @brief Clear the cached_frame_ queue
	 * @return false if any errors occurs
	 */
	bool flush_cached();

	/**
	 * @fn int frequency (void)
	 * @brief Access to frequency at which frames are received.
	 *        Relevant for the continuous_mode of the LZR Flatscan
	 * @return Return the frequency as an integer
	 **/
	int frequency() const;

	/**
	 * @brief Accessor to memorised parameters of Flatscan. Does NOT ask the actual parameters to the Flatscan. See get_parameters() for this
	 * Usefull if you know that you didn't modified the parameters
	 * @return Parameters object
	 */
	Parameters parameters();

	/**
	 * @brief The time used to take a scan.
	 * - 10.75ms in HS mode
	 * - 43ms in HD mode
	 * @return time in seconds
	 */
	float scan_time();

	/**
	 * @brief Overload operator<< to dump the actual state of the Flatscan
	 */
	friend std::ostream& operator<<(std::ostream& os, const Flatscan& fs);
private:
	Decoder* decoder_;
	Encoder* encoder_;

	std::future<Frame> timeout_wrapper_;

	size_t mdi_header_size_;

	Parameters parameters_;
	MDI_transfer_mode mdi_mode_;
	float angle_step_;
	uint32_t can_;

	boost::circular_buffer_space_optimized<Frame> cached_frame_;

	Frame search_frame(uint16_t cmd, unsigned int nb_try = 5);

	void update_config();

	bool init(unsigned int nb_try = 2);
};

#endif
