# Dependencies
- [boost](https://www.boost.org/ "Boost library")
- [serial from wjwwood](https://github.com/wjwwood/serial "wjwwood library")

# Compile
- `mdir build`
- `cmake ../`
- `cmake --build`

Once it's done, you will see a `lib` folder containing the shared library and an executable of the .cpp in dvlp folder
