#include <iostream>
#include "flatscan.h"

int main() {
    /** Init Flatscan object */
    Flatscan * fs;

    try {
        fs = new Flatscan("/dev/ttyUSB0");
    }
    catch (std::string e) {
		stc::cout << e << std::endl;
		return 1;
	}

    std::cout 

    /** set parameters */
    Parameters p = fs->parameters();

    p.set_mdi(MDI_DIST);
    p.set_acquisition(HS);
    ...

    fs->set_parameters(p);

    /** Retreive measurments */
    std::vector<Pol> p;
    while(1) {
        if(fs->measurments(p)) {
            /** do something with p */
        }
    }

    delete fs;

    return 0;
}