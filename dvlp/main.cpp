#include <iostream>
#include <unistd.h>

#include "flatscan.h"
#include "decoder.h"
#include "frame.h"
#include "parameters.h"

//#define DECODER
//#define FLATSCAN
//#define PARAMETERS
//#define ENCODER
#define CAN

using namespace std::chrono;

int main() {

	#ifdef DECODER
	serial::Serial* ser;

	try {
		ser = new serial::Serial("/dev/ttyUSB0", 921600);
	}
	catch(serial::IOException &e) {
		std::cout << e.what() << std::endl;
	}

	if(!ser->isOpen()) {
		std::cout << "Cannot open file descriptor" << std::endl;
		return -1;
	}

	Decoder decoder(*ser);
	Decoder::Error e;

	while (1) {
		if (decoder.error(e)) {
			std::cout << "Catch an error :(\nerr(" << e << ")\n";

			sleep(1);
		}

		if (decoder.has_data()) {
			Frame f = decoder.frame();

			f.to_string();

			std::cout << "Frequency : " << decoder.frequency() << " hz\n---\n";
		}
	}
	#endif

	#ifdef FLATSCAN
	Flatscan* fs;

	try {
		fs = new Flatscan("/dev/ttyUSB0", 921600);
	}
	catch(std::string &e) {
		std::cout << e << std::endl;
		return -1;
	}

	std::cout << *fs << std::endl;

	Parameters p = fs->parameters();
	p.set_averaging(Averaging::AVRG_DISABLE);
	p.set_acquisition(Acquisition::HS);
	p.set_number_spot(100);
	p.set_angle(0, 108);
	fs->set_parameters(p);

	std::cout << *fs << std::endl;

	sleep(3);

	std::vector<Rect> vec;

	// Set the size of output image to 1200x780 pixels
    //plt::figure_size(1200, 780);

	std::vector<double> x, y;

	while(1) {
		if (fs->get_measurment(vec)) {
			for (auto it = vec.begin(); it != vec.end(); ++it) {
				x.push_back(it->x / 1000L);
				y.push_back(it->y / 1000L);
			}
			plt::clf();
			plt::xlim(-1, 4);
			plt::ylim(0, 4);
			plt::plot(x, y);
			plt::pause(0.01);
			vec.clear();
			x.clear();
			y.clear();
		}
	}
	#endif

	#ifdef PARAMETERS
	Parameters p;

	/*p.set_ctn(ENABLE);
	p.set_acquisition(HS);
	p.set_mdi(MDI_DIST);
	p.set_number_spot(99);
	p.set_averaging(AVRG_3PTS);*/

	std::cout << p;
	#endif

	#ifdef ENCODER
	Flatscan* fs;

	try {
		fs = new Flatscan("/dev/ttyUSB0", 921600);
	}
	catch(std::string &e) {
		std::cout << e << std::endl;
		return -1;
	}

	std::cout << *fs << std::endl;

	fs->set_mdi_transfer_mode(MDI_transfer_mode::CONTINUOUS);

	std::vector<Rect> r;

	//fs->get_measurments();
	while(1) {
		if (fs->measurments(r)) {
			r.clear();

			/*if (++i < 10)
				fs->get_measurments();*/

			std::cout << "got something\n";
		}
	}

	#endif

	#ifdef CAN
	Flatscan* fs;

	try {
		fs = new Flatscan("/dev/ttyUSB0", 921600);
	}
	catch(std::string &e) {
		std::cout << e << std::endl;
		return -1;
	}

	std::cout << *fs << std::endl;

	std::cout << "CAN number : " << (int)fs->get_can() << std::endl;

	#endif

	return 0;
}
